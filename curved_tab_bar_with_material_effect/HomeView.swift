//
//  HomeView.swift
//  curved_tab_bar_with_material_effect
//
//  Created by Kitti Jarearnsuk on 12/9/2565 BE.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
