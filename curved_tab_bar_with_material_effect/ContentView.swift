//
//  ContentView.swift
//  curved_tab_bar_with_material_effect
//
//  Created by Kitti Jarearnsuk on 12/9/2565 BE.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabcurveView().preferredColorScheme(.light)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
