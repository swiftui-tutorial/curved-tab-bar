//
//  TabcurveView.swift
//  curved_tab_bar_with_material_effect
//
//  Created by Kitti Jarearnsuk on 12/9/2565 BE.
//

import SwiftUI

struct TabcurveView: View {
    @State var currentTab = "leaf"
    @State var curveAxis: CGFloat = 0
    
    init(){
        UITabBar.appearance().isHidden = true
    }
    
    var body: some View {
        VStack(spacing: 0){
            TabView(selection: $currentTab){
                HomeView()
                    .tag("leaf")
                Text("Search")
                    .tag("magnifyingglass")
                Text("Account")
                    .tag("person")
            }
            .clipShape(
              CustomTabCurve(curveAxis: curveAxis)
            )
            .padding(.bottom, -90)
            
            HStack(spacing: 0) {
                TabButtons()
            }
            .frame(height: 50)
            .padding(.horizontal, 35)
            .padding(.bottom, getSafeArea().bottom == 0 ? 20 : 0)
        }
        .background(Color("Green"))
        .ignoresSafeArea(.container, edges: .top)
    }
    
    @ViewBuilder
    func TabButtons()-> some View {
        ForEach(["leaf", "magnifyingglass", "person"], id: \.self) { image in
            GeometryReader { proxy in
                Button(action: {
                    withAnimation{
                        currentTab = image
                        curveAxis = proxy.frame(in: .global).midX
                    }
                }, label: {
                    Image(systemName: image)
                        .font(.title2)
                        .foregroundColor(Color.white)
                        .frame(width: 45, height: 45)
                        .background(
                            Circle().fill(Color("Green"))
                        ).offset( y: currentTab == image ? -25 : 0)
                })
                .frame(maxWidth: .infinity, alignment: .center)
                .onAppear(perform: {
                    if curveAxis == 0 && image == "leaf" {
                        curveAxis = proxy.frame(in: .global).midX
                    }
                })
            }
            .frame( height: 40)
        }
    }
}

struct TabcurveView_Previews: PreviewProvider {
    static var previews: some View {
        TabcurveView()
    }
}

extension View {
    func getRect() -> CGRect {
        UIScreen.main.bounds
    }
    
    func getSafeArea()->UIEdgeInsets {
        guard let screen = UIApplication.shared.connectedScenes.first as? UIWindowScene else {
            return .zero
        }
        
        guard let safeArea = screen.windows.first?.safeAreaInsets else {
            return .zero
        }
        
        return safeArea
    }
}
