//
//  curved_tab_bar_with_material_effectApp.swift
//  curved_tab_bar_with_material_effect
//
//  Created by Kitti Jarearnsuk on 12/9/2565 BE.
//

import SwiftUI

@main
struct curved_tab_bar_with_material_effectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
